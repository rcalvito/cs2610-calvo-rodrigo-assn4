// Set the document title
document.title = 'TODO: Dynamic Fibonacci Sequence in JavaScript';

// Create a red div in the body
var div = document.createElement('div');
div.setAttribute('class', 'red shadowed stuff-box');
document.querySelector('body').appendChild(div);
var divPar = document.createElement('p');

var div2 = document.createElement('div');
div2.setAttribute('id','div2');

div2.setAttribute('class', 'green shadowed stuff-box');
document.querySelector('body').appendChild(div2);


function iterFib(number){
	var case0 = 0;
	var case1 = 1;
	var temp;
	while(number>=0){
		temp=case1;
		case1=case1+case0;
		case0=temp;
		number--;
	}
	return case0;

}
function makePar(number){
	
	var i;
	divPar.textContent= "0 ";
	for(i=0;i<number;i++){
		divPar.textContent+=iterFib(i).toString();
		divPar.textContent+= " ";
	}
	return;
}

function createDiv(number){
	
	let div = document.createElement('div');
	let par = document.createElement('p');
	div.setAttribute('class', 'fib');
	par.textContent = "Fib("+number+") =" + iterFib(number-1);
	div.appendChild(par);
	return div;
}

function createDivLeft(number){
	
	let div = document.createElement('div');
	let par = document.createElement('p');
	div.setAttribute('class', 'fib fib-left');
	par.textContent = "Fib("+number+") =" + iterFib(number-1);
	div.appendChild(par);
	return div;
}

function createDivRight(number){
	let div = document.createElement('div');
	let par = document.createElement('p');
	div.setAttribute('class', 'fib fib-right');
	par.textContent = "Fib("+number+") =" + iterFib(number-1);
	div.appendChild(par);
	return div;
}

function makeTree(number, div){
	if(number-1<0){
		return;
	}
	else if(number==0){;
		div.appendChild(createDivLeft(0));
		return;
	}
	else if(number==1){
		div.appendChild(createDivLeft(0));
		return;
	}
	else{
		let divLeft = createDivLeft((number-1));
		let divRight = createDivRight((number-2));
		div.appendChild(divLeft);
		div.appendChild(divRight);
		makeTree(number-1,divLeft);
		makeTree(number-2,divRight);
	}
	return div;

}




// Add instructions
var para = document.createElement('p');
var slider = document.createElement('input');
var slider2 = document.createElement('input');
var label = document.createElement('label');
var label2 = document.createElement('label');

slider.setAttribute('type', 'range');
slider.setAttribute('min','0');
slider.setAttribute('max','50');
slider.setAttribute('value','0');
slider.setAttribute('id','slider');
label.setAttribute('for', 'slider');
label.textContent = "Fib(0) ";

slider2.setAttribute('type', 'range');
slider2.setAttribute('min','0');
slider2.setAttribute('max','11');
slider2.setAttribute('value','0');
slider2.setAttribute('id','slider2');
label2.setAttribute('for','slider2');
label2.textContent = "Fib(0) ";


div.appendChild(label);
div.appendChild(slider);
div.appendChild(divPar);

div2.appendChild(label2);
div2.appendChild(slider2);
div2.appendChild(document.createElement('br'));
var tempDiv =document.createElement('div');
tempDiv = createDiv(0);
tempDiv.setAttribute('id','tempDiv');
div2.appendChild(tempDiv);

slider.oninput = function (){
	label.textContent = "Fib(" + this.value + ") ";
	makePar(this.value);
}
slider2.oninput = function(){

	label2.textContent = "Fib(" + this.value + ") ";
	var uno= document.getElementById("div2"); //sorry for the name variables, I was tired and could not think of anything
	uno.removeChild(uno.lastChild);
	var dos =createDiv(this.value);
	makeTree(this.value,dos);
	div2.appendChild(dos);
	

}

